﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{

    [Header("Set Dynamically")]
    public Rigidbody rigid;


    void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        pos = Random.insideUnitSphere * Spawner.S.spawnRadius;

        Vector3 vel = Random.onUnitSphere * Spawner.S.velocity;
        rigid.velocity = vel;

        LookAhead();

        //окрасить птицу в случайный цвет но не слишкоом темный
        Color randColor = Color.black;
        while (randColor.r + randColor.g + randColor.b < 1.0f)
        {
            randColor = new Color(Random.value, Random.value, Random.value);
        }

        Renderer[] rends = gameObject.GetComponentsInChildren<Renderer>();//вернет массив подключенных а этому объекту и всех потомков
        foreach (Renderer r in rends)
        {
            r.material.color = randColor;
        }

        TrailRenderer tRend = GetComponent<TrailRenderer>();
        tRend.material.SetColor("_TintColor", randColor);


    }

    void LookAhead(){
        transform.LookAt(pos + rigid.velocity);//ориентирует клювом в сторону полета
    }

    public Vector3 pos
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    private void FixedUpdate()
    {
        Vector3 vel = rigid.velocity;
        Spawner spn = Spawner.S;

        //притяжение - организовать движение к аттрактору
        Vector3 delta = Attractor.POS - pos;

        //проверть куда двигаться в сторону аттрактора или от него
        bool attracted = (delta.magnitude > spn.attractPushDist);
        Vector3 velAttract = delta.normalized * spn.velocity;

        //применить все скорости
        float fdt = Time.fixedDeltaTime;

        if (attracted)
        {
            vel = Vector3.Lerp(vel, velAttract, spn.attractPull * fdt);
        } else
        {
            vel = Vector3.Lerp(vel, -velAttract, spn.attractPush * fdt);
        }

        //установить vel в соответствии с velocity в объекте-одиночке Spawner
        vel = vel.normalized * spn.velocity;

        //присвоить скорость компоненту риджибади
        rigid.velocity = vel;

        //повернуть птицу в сторону нового направленя
        LookAhead();
    }

}
